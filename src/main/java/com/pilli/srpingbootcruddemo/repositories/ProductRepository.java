package com.pilli.srpingbootcruddemo.repositories;

import com.pilli.srpingbootcruddemo.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}