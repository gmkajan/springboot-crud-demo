package com.pilli.srpingbootcruddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrpingbootCrudDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrpingbootCrudDemoApplication.class, args);
    }

}
